package com.example.classactivity4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EndActvity extends AppCompatActivity {

    Button b;
    private TextView name, job, address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_actvity);

        b = (Button) findViewById(R.id.back);
        name = findViewById(R.id.showName);
        job = findViewById(R.id.showJob);
        address = findViewById(R.id.showAddress);

        // setting button listener
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EndActvity.this, MainActivity.class);
                startActivity(i);
            }
        });

        String _name = getIntent().getStringExtra("name");
        String _job = getIntent().getStringExtra("job");
        String _address = getIntent().getStringExtra("address");

        if (!_name.equals("")) {
            name.setText(_name);
        }
        else {
            name.setText("?");
        }

        if (!_job.equals("")) {
            job.setText(_job);
        }
        else {
            job.setText("?");
        }

        if (!_address.equals("")) {
            address.setText(_address);
        }
        else {
            address.setText("?");
        }
    }
}