package com.example.classactivity4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private Button b;
    private EditText name, job, address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b = (Button) findViewById(R.id.button);
        name = findViewById(R.id.editTextName);
        job = findViewById(R.id.editTextJobTitle);
        address = findViewById(R.id.editTextAddress);

        // setting listener on button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String n = name.getText().toString();
                String j = job.getText().toString();
                String add = address.getText().toString();

                Intent i = new Intent(MainActivity.this, EndActvity.class);
                i.putExtra("name", n);
                i.putExtra("job", j);
                i.putExtra("address", add);

                startActivity(i);
            }
        });
    }

//     resetting states when activity starts
    @Override
    protected void onStart() {
        super.onStart();
        name.setText(null);
        job.setText(null);
        address.setText(null);
    }
}